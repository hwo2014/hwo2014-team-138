﻿using System;
using MrBotLibrary;
using MrBotLibrary.Output;

namespace MrBotConsole
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			if (args.Length == 0)
			{
				//string host = "testserver.helloworldopen.com";
				var availableHosts = new[]
				{
					"testserver.helloworldopen.com",
					//"hakkinen.helloworldopen.com",
					//"senna.helloworldopen.com",
					//"webber.helloworldopen.com"
				};
				var host = availableHosts[new Random().Next(0, availableHosts.Length)];
				int port = 8091;
				string botName = "new Team";
				string botKey = "VRg3wbRgFxi+4A";
				Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
				var bot = new Bot(botName, botKey);
				bot.AddOutput(new ConsoleOutput(5, 15));
				bot.AddOutput(new CsvOutput("C:\\hwo2014log"));
				bot.JoinRace(host, port, "keimola", null, 1);
			}
				// this is for CI
			else if (args.Length == 4)
			{
				string host = args[0];
				int port = int.Parse(args[1]);
				string botName = args[2];
				string botKey = args[3];
				Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
				var bot = new Bot(botName, botKey);
				bot.AddOutput(new EventConsoleOutput());
				bot.Join(host, port);
			}
			else
			{
				string host = args[0];
				int port = int.Parse(args[1]);
				string botName = args[2];
				string botKey = "VRg3wbRgFxi+4A";
				string track = args[3];
				int carCount = int.Parse(args[4]);
				Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " using track " + track + " and car count " + carCount);
				var bot = new Bot(botName, botKey);
				bot.AddOutput(new ConsoleOutput(5, 15));
				bot.JoinRace(host, port, track, null, carCount);
			}
		}
	}
}