namespace MrBotLibrary.ApiReponses
{
	public class BestLap
	{
		public CarId Car { get; set; }
		public LapTime Result { get; set; }
		
	}
}