﻿namespace MrBotLibrary.ApiReponses
{
	public class Car
	{
		public CarId Id { get; set; }
		public CarDimensions Dimensions { get; set; }
	}
}