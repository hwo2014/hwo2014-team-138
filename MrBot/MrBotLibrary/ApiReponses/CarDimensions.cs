namespace MrBotLibrary.ApiReponses
{
	public class CarDimensions
	{
		public double Length { get; set; }
		public double Width { get; set; }
		public double GuideFlagPosition { get; set; }
	}
}