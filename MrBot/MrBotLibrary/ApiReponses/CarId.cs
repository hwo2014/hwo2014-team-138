﻿using Newtonsoft.Json;

namespace MrBotLibrary.ApiReponses
{
	public class BotId
	{
		[JsonProperty(PropertyName = "name")]
		public string Name;
		[JsonProperty(PropertyName = "key")]
		public string Key;

		public BotId(string name, string key)
		{
			Name = name;
			Key = key;
		}
	}
	public class CarId
	{
		public string Name { get; set; }
		public string Color { get; set; }

		protected bool Equals(CarId other)
		{
			return string.Equals(Name, other.Name) && string.Equals(Color, other.Color);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Name != null ? Name.GetHashCode() : 0)*397) ^ (Color != null ? Color.GetHashCode() : 0);
			}
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((CarId) obj);
		}
	}
}