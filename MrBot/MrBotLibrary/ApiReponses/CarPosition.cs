﻿namespace MrBotLibrary.ApiReponses
{
	public class CarPosition
	{
		public int GameTick { get; set; }
		public CarId Id { get; set; }
		public double Angle { get; set; }
		public PiecePosition PiecePosition { get; set; }
		public double? Speed { get; set; }
		public double? Throttle { get; set; }
		public string ThrottleCalculator { get; set; }
		public double? Acceleration { get; set; }
	}
}