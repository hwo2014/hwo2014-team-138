﻿namespace MrBotLibrary.ApiReponses
{
	public class Dnf
	{
		public CarId Car { get; set; }
		public string Reason { get; set; }
	}
}