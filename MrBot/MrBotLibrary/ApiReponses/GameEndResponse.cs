﻿namespace MrBotLibrary.ApiReponses
{
	public class GameEndResponse
	{
		public RaceResult[] Results { get; set; }
		public BestLap[] BestLaps { get; set; }
	}
}