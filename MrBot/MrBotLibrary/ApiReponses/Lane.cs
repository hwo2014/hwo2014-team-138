﻿namespace MrBotLibrary.ApiReponses
{
	public class Lane
	{
		protected bool Equals(Lane other)
		{
			return Index == other.Index;
		}

		public override int GetHashCode()
		{
			return Index;
		}

		public int DistanceFromCenter { get; set; }
		public int Index { get; set; }

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Lane) obj);
		}
	}
}