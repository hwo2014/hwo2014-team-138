﻿namespace MrBotLibrary.ApiReponses
{
	public class LaneIndices
	{
		public int StartLaneIndex { get; set; }
		public int EndLaneIndex { get; set; }
	}
}