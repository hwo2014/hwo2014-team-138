﻿namespace MrBotLibrary.ApiReponses
{
	public class LanePosition
	{
		public int StartLaneIndex { get; set; }
		public int EndLaneIndex { get; set; }
	}
}