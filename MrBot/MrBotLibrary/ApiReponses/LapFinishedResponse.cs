﻿namespace MrBotLibrary.ApiReponses
{
	public class LapFinishedResponse
	{
		public CarId Car { get; set; }
		public LapTime LapTime { get; set; }
		public RaceTime RaceTime { get; set; }
		public Ranking Ranking { get; set; }
	}
}