﻿namespace MrBotLibrary.ApiReponses
{
	public class LapTime
	{
		public int Lap { get; set; }
		public int Ticks { get; set; }
		public int Millis { get; set; }
	}
}