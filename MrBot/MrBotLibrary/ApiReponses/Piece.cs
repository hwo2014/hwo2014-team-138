﻿namespace MrBotLibrary.ApiReponses
{
	using System.Collections.Generic;

	public class Piece
	{
		public double Length { get; set; }
		public double Radius { get; set; }
		public double Angle { get; set; }
		public bool Switch { get; set; }
		public Track Track { get; set; }
		public List<Lane> ShortestLanes { get; set; }
		public bool IsOnHomeStrech { get; set; }
	}
}