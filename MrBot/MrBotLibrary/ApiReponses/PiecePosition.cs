﻿namespace MrBotLibrary.ApiReponses
{
	public class PiecePosition
	{
		public int PieceIndex { get; set; }
		public double InPieceDistance { get; set; }
		public LanePosition Lane { get; set; }
		public int Lap { get; set; }
	}
}