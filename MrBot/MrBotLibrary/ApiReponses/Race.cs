﻿namespace MrBotLibrary.ApiReponses
{
	public class Race
	{
		public Track Track { get; set; }
		public Car[] Cars { get; set; }
		public RaceSession RaceSession { get; set; }
	}
}