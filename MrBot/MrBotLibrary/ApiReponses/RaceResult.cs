﻿namespace MrBotLibrary.ApiReponses
{
	public class RaceResult
	{
		public CarId Car { get; set; }
		public RaceTime Result { get; set; }
	}
}