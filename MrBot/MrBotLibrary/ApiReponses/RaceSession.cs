﻿namespace MrBotLibrary.ApiReponses
{
	public class RaceSession
	{
		public int Laps { get; set; }
		public int MaxLapTimeMs { get; set; }
		public bool QuickRace { get; set; }
	}
}