﻿namespace MrBotLibrary.ApiReponses
{
	public class RaceTime
	{
		public int Laps { get; set; }
		public int Ticks { get; set; }
		public int Millis { get; set; }
	}
}