﻿namespace MrBotLibrary.ApiReponses
{
	public class Ranking
	{
		public int Overall { get; set; }
		public int FastestLap { get; set; }
	}
}