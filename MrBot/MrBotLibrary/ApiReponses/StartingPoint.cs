﻿namespace MrBotLibrary.ApiReponses
{
	public class StartingPoint
	{
		public Position Position { get; set; }
		public double Angle { get; set; }
	}
}