﻿namespace MrBotLibrary.ApiReponses
{
	public class Track
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public Piece[] Pieces { get; set; }
		public Lane[] Lanes { get; set; }
		public StartingPoint StartingPoint { get; set; }
		public Piece StartOfLongestStraightLine { get; set; }
	}
}