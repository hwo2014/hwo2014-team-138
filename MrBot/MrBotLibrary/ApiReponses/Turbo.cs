﻿namespace MrBotLibrary.ApiReponses
{
	public class Turbo
	{
		public double TurboDurationMilliseconds { get; set; }
		public int TurboDurationTicks { get; set; }
		public double TurboFactor { get; set; }
		public bool Active { get; set; }
		public int ActivationTick { get; set; }
	}
}