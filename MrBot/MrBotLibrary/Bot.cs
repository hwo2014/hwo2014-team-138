﻿using MrBotLibrary.Calculators.SwitchLane;
using MrBotLibrary.Calculators.Throttle;

namespace MrBotLibrary
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Globalization;
	using System.IO;
	using System.Linq;
	using System.Net.Sockets;

	using ApiReponses;
	using Extensions;
	using Interfaces;

	using MrBotLibrary.Calculators.Turbo;

	using Output;

	using Newtonsoft.Json;

	public class Bot : IWin
	{
		protected StreamWriter Writer;
		protected readonly string BotName;
		protected readonly string BotKey;
		private readonly List<IOutput> outputs;
		private readonly List<IThrottleCalculator> throttleCalculators;
		private readonly List<ISwitchLaneCalculator> switchLaneCalculators;
		private readonly List<ITurboCalculator> turboCalculators;
		private RaceData raceData;

		public Bot(string botName, string botKey)
		{
			BotName = botName;
			BotKey = botKey;
			outputs = new List<IOutput>();
			throttleCalculators = new List<IThrottleCalculator>();
			switchLaneCalculators = new List<ISwitchLaneCalculator>();
			turboCalculators = new List<ITurboCalculator>();
			raceData = new RaceData();
			Reset();
		}

		private void RegisterThrottleCalculators()
		{
			throttleCalculators.Clear();
			// Order is important, calculators will be called in that order and first one to return a throttle wins.
			throttleCalculators.Add(new FinishingLineThrottleCalculator(raceData));
			throttleCalculators.Add(new GatherDataThrottleCalculator(raceData));
			throttleCalculators.Add(new AggressiveThrottleCalculator(raceData));
			throttleCalculators.Add(new DefensiveThrottleCalculator(raceData));
			throttleCalculators.Add(new DriftThrottleCalculator(raceData));
		}
		private void RegisterShortLaneCalculators()
		{
			switchLaneCalculators.Clear();
			// Order is important, calculators will be called in that order and first one to return a throttle wins.
			switchLaneCalculators.Add(new AggressiveSwitchLaneCalculator(raceData));
			switchLaneCalculators.Add(new DefensiveSwitchLaneCalculator(raceData));
			switchLaneCalculators.Add(new ShortestPathSwitchLaneCalculator(raceData));
		}
		private void RegisterTurboCalculators()
		{
			turboCalculators.Clear();
			// Order is important, calculators will be called in that order and first one to return a throttle wins.
			turboCalculators.Add(new FinishingLaneTurboCalculator(raceData));
			turboCalculators.Add(new AggressiveTurboCalculator(raceData));
			turboCalculators.Add(new CasualTurboCalculator(raceData));
		}
		protected void Reset()
		{
			raceData.CarPositionCache.Clear();
			raceData.CrashedCars.Clear();
			raceData.Turbo = null;
			RegisterThrottleCalculators();
			RegisterShortLaneCalculators();
			RegisterTurboCalculators();
		}

		public void AddOutput(IOutput output)
		{
			outputs.Add(output);
		}

		protected void Win(string host, int port, SendMsg winMessage)
		{
			try
			{
				using (var client = new TcpClient(host, port))
				{
					var stream = client.GetStream();
					var reader = new StreamReader(stream);
					Writer = new StreamWriter(stream)
					{
						AutoFlush = true
					};

					string line;
					Send(winMessage);
					while ((line = reader.ReadLine()) != null)
					{
						HandleMessage(line);
					}
					outputs.ForEach(x => x.AddEvent("Good bye"));
					outputs.ForEach(x => x.Flush());
					Console.ReadKey();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Unhandled Exception: {0} - {1}", ex.Message, ex.StackTrace);
				Debug.WriteLine("Unhandled Exception: {0} - {1}", ex.Message, ex.StackTrace);
			}
		}
		public void Join(string host, int port)
		{
			Win(host, port, new Join(BotName, BotKey));
		}
		public void JoinRace(string host, int port, string trackName, string password, int carCount)
		{
			Win(host, port, new JoinRace(BotName, BotKey, trackName, password, carCount));
		}

		private void HandleMessage(string messageJson)
		{
			var msg = JsonConvert.DeserializeObject<MsgWrapper>(messageJson);
			Debug.WriteLine("{0}: {1}", msg.msgType, msg.data);
			SendMsg response = null;
			switch (msg.msgType)
			{
				case "yourCar":
					var yourCar = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
					response = HandleYourCar(yourCar);
					break;
				case "carPositions":
					var carPositions = JsonConvert.DeserializeObject<CarPosition[]>(msg.data.ToString());
					response = HandleCarPositions(carPositions, msg.gameTick.GetValueOrDefault());
					break;
				case "join":
					HandleJoined();
					break;
				case "gameInit":
					var gameInitResponse = JsonConvert.DeserializeObject<GameInitResponse>(msg.data.ToString());
					response = HandleGameInit(gameInitResponse);
					break;
				case "gameEnd":
					var gameEndResponse = JsonConvert.DeserializeObject<GameEndResponse>(msg.data.ToString());
					response = HandleGameEnd(gameEndResponse);
					break;
				case "gameStart":
					response = HandleGameStart();
					break;
				case "tournamentEnd":
					response = HandleTournamentEnd();
					break;
				case "crash":
					var crash = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
					response = HandleCrash(crash, msg.gameTick.GetValueOrDefault());
					outputs.ForEach(x => x.Flush());
					break;
				case "spawn":
					var spawn = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
					response = HandleSpawn(spawn, msg.gameTick.GetValueOrDefault());
					break;
				case "lapFinished":
					var lapFinishedResponse = JsonConvert.DeserializeObject<LapFinishedResponse>(msg.data.ToString());
					response = HandleLapFinished(lapFinishedResponse);
					break;
				case "dnf":
					var dnf = JsonConvert.DeserializeObject<Dnf>(msg.data.ToString());
					response = HandleDnf(dnf);
					break;
				case "finished":
					response = HandleFinished();
					break;
				case "turboAvailable":
					var turboAvailableResponse = JsonConvert.DeserializeObject<Turbo>(msg.data.ToString());
					response = HandleTurboAvailable(turboAvailableResponse);
					break;
				case "error":
					response = HandleError(msg.data.ToString());
					break;
			}
			// only send response when message contained gameTick http://www.reddit.com/r/HWO/comments/23j31d/throttle_delayed_by_1_tick_or_not/
			if (msg.gameTick.HasValue || response != null)
			{
				response = response ?? new Ping();
				Send(response);
			}
		}
		private SendMsg HandleTurboAvailable(Turbo turboAvailableResponse)
		{
			outputs.ForEach(x => x.AddEvent("Turbo available"));
			raceData.Turbo = turboAvailableResponse;
			raceData.LastSeenTurbo = turboAvailableResponse;
			return null;
		}

		private SendMsg HandleError(string error)
		{
			outputs.ForEach(x => x.AddEvent(String.Format("Server returned an error: {0}", error)));
			return null;
		}

		private SendMsg HandleFinished()
		{
			outputs.ForEach(x => x.AddEvent("Someone finished"));
			return null;
		}

		private SendMsg HandleDnf(Dnf dnf)
		{
			if (!raceData.CrashedCars.Contains(dnf.Car))
			{
				raceData.CrashedCars.Add(dnf.Car);
			}
			outputs.ForEach(x => x.AddEvent(String.Format("{0} disqualified ({1})", dnf.Car.Name, dnf.Reason)));
			return null;
		}

		private SendMsg HandleLapFinished(LapFinishedResponse lapFinishedResponse)
		{
			outputs.ForEach(x => x.AddEvent(String.Format("{0} finished a lap after {1} ms", lapFinishedResponse.Car.Name, lapFinishedResponse.LapTime.Millis)));
			return null;
		}

		private SendMsg HandleSpawn(CarId spawn, int gameTick)
		{
			if (raceData.CrashedCars.Contains(spawn))
			{
				raceData.CrashedCars.Remove(spawn);
			}
			outputs.ForEach(x => x.AddEvent(String.Format("{0} spawned", spawn.Name)));
			return null;
		}

		private SendMsg HandleCrash(CarId crash, int gameTick)
		{
			var lastPosition = raceData.CarPositionCache[crash].LastOrDefault();
			if (lastPosition != null)
			{
				raceData.MaxAngle = Math.Max(raceData.MaxAngle.GetValueOrDefault(), Math.Abs(lastPosition.Angle));
			}
			if (!raceData.CrashedCars.Contains(crash))
			{
				raceData.CrashedCars.Add(crash);
			}
			outputs.ForEach(x => x.AddEvent(String.Format("{0} crashed, max angle on this track so far: {1}", crash.Name, raceData.MaxAngle.HasValue ? raceData.MaxAngle.Value.ToString(CultureInfo.InvariantCulture) : "unknown")));
			return null;
		}

		private SendMsg HandleTournamentEnd()
		{
			outputs.ForEach(x => x.AddEvent("Tournament ended"));
			return null;
		}

		private SendMsg HandleGameStart()
		{
			outputs.ForEach(x => x.AddEvent("Race starts"));
			return new Ping();
		}

		private SendMsg HandleGameEnd(GameEndResponse gameEndResponse)
		{
			outputs.ForEach(x => x.AddEvent("Race ended"));
			outputs.ForEach(x => x.AddEvent(String.Format("Max seen angle on this track: {0}", raceData.MaxAngle)));
			return null;
		}

		private SendMsg HandleGameInit(GameInitResponse gameInitResponse)
		{
			Reset();
			outputs.ForEach(x => x.AddEvent("Race init"));
			foreach (Car car in gameInitResponse.Race.Cars)
			{
				raceData.CarPositionCache.Add(car.Id, new List<CarPosition>());
			}
			raceData.Track = gameInitResponse.Race.Track;
			foreach (var piece in raceData.Track.Pieces)
			{
				piece.Track = raceData.Track;
			}
			foreach (var piece in raceData.Track.Pieces)
			{
				piece.ShortestLanes = piece.GetShortestLanes();
			}
			raceData.Track.CalculateHomeStrech();
			raceData.Track.CalculateLongestStraightLine();
			raceData.RaceSession = gameInitResponse.Race.RaceSession;
			return null;
		}

		private void HandleJoined()
		{
			outputs.ForEach(x => x.AddEvent("Joined"));
		}

		private SendMsg HandleYourCar(CarId yourCar)
		{
			raceData.Self = yourCar;
			return null;
		}

		protected virtual SendMsg HandleCarPositions(IList<CarPosition> carPositions, int gameTick)
		{
			UpdateRaceData(carPositions, gameTick);

			double? throttle = null;
			foreach (var throttleCalculator in throttleCalculators)
			{
				throttle = throttleCalculator.Calculate();
				if (throttle.HasValue)
				{
					OwnCarPosition.ThrottleCalculator = throttleCalculator.Name;
					break;
				}
			}

			string switchLane = null;
			string switchLaneCalculatorName = null;
			foreach (var switchLaneCalculator in switchLaneCalculators)
			{
				switchLane = switchLaneCalculator.Calculate();
				if (switchLane != null)
				{
					switchLaneCalculatorName = switchLaneCalculator.GetType().Name;
					break;
				}
			}

			bool? turbo = null;
			string turboCalculatorName = null;
			if (raceData.Turbo != null && !raceData.Turbo.Active)
			{
				foreach (var turboCalculator in turboCalculators)
				{
					turbo = turboCalculator.Calculate();
					if (turbo.HasValue)
					{
						turboCalculatorName = turboCalculator.GetType().Name;
						break;
					}
				}
			}

			SendMsg response = null;

			if (switchLane != null && (LastCarPosition == null || OwnCarPosition.PiecePosition.PieceIndex != LastCarPosition.PiecePosition.PieceIndex))
			{
				outputs.ForEach(x => x.AddEvent(String.Format("Switching lanes to the {0}, requested by {1}", switchLane, switchLaneCalculatorName)));
				Debug.WriteLine("Tick {0}: Switching lanes to the {1}, requested by {2}", gameTick, switchLane, switchLaneCalculatorName);
				response = new SwitchLane(switchLane);
			}
			else if (turbo.HasValue && turbo.Value)
			{
				outputs.ForEach(x => x.AddEvent(String.Format("Using turbo, requested by {0}", turboCalculatorName)));
				response = UseTurbo(gameTick);
			}
			else if (throttle.HasValue)
			{
				OwnCarPosition.Throttle = throttle.Value;
				response = new Throttle(throttle.Value);
			}

			if (response == null || response.GetType() != typeof(Throttle))
			{
				OwnCarPosition.Throttle = LastCarPosition != null ? LastCarPosition.Throttle : null;
			}

			AddDataToOutputs(gameTick);
			outputs.ForEach(x => x.OnGametick());
			return response;
		}

		private void UpdateRaceData(IList<CarPosition> carPositions, int gameTick)
		{
			foreach (CarPosition carPosition in carPositions)
			{
				carPosition.GameTick = gameTick;
				raceData.CarPositionCache[carPosition.Id].Add(carPosition);
			}
			if (raceData.Turbo != null && raceData.Turbo.Active && raceData.Turbo.GetRemainingTicks(gameTick) <= 0)
			{
				raceData.Turbo = null;
			}
			var maxAngleOfCurrentPositions = carPositions.Max(x => Math.Abs(x.Angle));
			if (maxAngleOfCurrentPositions > raceData.MaxAngle.GetValueOrDefault())
			{
				raceData.MaxAngle = maxAngleOfCurrentPositions;
			}
		}
		private SendMsg UseTurbo(int gameTick)
		{
			outputs.ForEach(x => x.AddEvent("Using turbo"));
			raceData.Turbo.Active = true;
			raceData.Turbo.ActivationTick = gameTick;
			return new UseTurbo(@"Turbo! \o/");
		}
		private void AddDataToOutputs(int gameTick)
		{
			var output = new Dictionary<string, object>();
			output.Add("Tick", gameTick);
			output.Add("Trackangle", CurrentPiece.Angle);
			if (CurrentPiece.Radius > 0 && OwnCarPosition.Speed.HasValue)
			{
				var radius = CurrentPiece.GetRadius(CurrentLane);
				var centrifugalForce = (OwnCarPosition.Speed.Value * OwnCarPosition.Speed.Value) / radius;
				output.Add("Radius", radius);
				output.Add("CF", centrifugalForce.ToString("F4"));
			}
			else
			{
				output.Add("Radius", "N/A");
				output.Add("CF", "N/A");
			}
			output.Add("Angle", OwnCarPosition.Angle.ToString("F6"));
			if (LastCarPosition != null)
			{
				output.Add("AngleDiff", (OwnCarPosition.Angle - LastCarPosition.Angle).ToString("F6"));
			}
			output.Add("Throttle", OwnCarPosition.Throttle.GetValueOrDefault().ToString("F4"));
			output.Add("ThrottleCalc", OwnCarPosition.ThrottleCalculator);
			output.Add("Speed", OwnCarPosition.Speed.GetValueOrDefault().ToString("F4"));
			output.Add("Acceleration", OwnCarPosition.Acceleration.GetValueOrDefault().ToString("F4"));
			output.Add("Turbo",
				raceData.Turbo == null || !raceData.Turbo.Active ? "N/A" : raceData.Turbo.GetRemainingTicks(gameTick).GetValueOrDefault().ToString());
			outputs.ForEach(x => x.AddData(output));
		}
		protected void Send(SendMsg msg)
		{
			Writer.WriteLine(msg.ToJson());
		}

		public CarPosition OwnCarPosition { get { return raceData.OwnCarPosition(); } }
		public CarPosition LastCarPosition { get { return raceData.LastCarPosition(raceData.Self); } }
		public Piece CurrentPiece { get { return raceData.CurrentPiece(); } }
		public Lane CurrentLane { get { return raceData.CurrentLane(); } }

		#region Secret Weapon

		private readonly Exception banana = new Exception();

		#endregion
	}
}