﻿using System;
using MrBotLibrary.ApiReponses;
using MrBotLibrary.Extensions;

namespace MrBotLibrary.Calculators.Base
{
	public class OpponentAwareCalculator
	{
		private readonly RaceData raceData;
		private readonly ThrottleCalculator throttleCalculator;

		public OpponentAwareCalculator(RaceData raceData)
		{
			this.raceData = raceData;
			throttleCalculator = new ThrottleCalculator(raceData);
		}

		public double CalculateDistance(PiecePosition fromPiecePosition, PiecePosition toPiecePosition)
		{
			var fromPiece = raceData.GetPiece(fromPiecePosition.PieceIndex);
			var fromLane = raceData.GetLane(fromPiecePosition.Lane.EndLaneIndex);
			var toPiece = raceData.GetPiece(toPiecePosition.PieceIndex);
			var toLane = raceData.GetLane(toPiecePosition.Lane.EndLaneIndex);
			var pieceDistance = fromPiece.DistanceTo(toPiece, toLane);
			double distance = pieceDistance - fromPiecePosition.InPieceDistance + toPiecePosition.InPieceDistance;
			if (distance < 0)
			{
				pieceDistance = fromPiece.DistanceTo(toPiece.Prev(), fromLane) + toPiece.GetLength(fromLane);
				distance = pieceDistance - fromPiecePosition.InPieceDistance + toPiecePosition.InPieceDistance;
			}
			return distance;
		}

		public bool CanReach(PiecePosition fromPiecePosition, PiecePosition toPiecePosition)
		{
			if (fromPiecePosition.Lane.StartLaneIndex == fromPiecePosition.Lane.EndLaneIndex && toPiecePosition.Lane.StartLaneIndex == toPiecePosition.Lane.EndLaneIndex && fromPiecePosition.Lane.EndLaneIndex == toPiecePosition.Lane.EndLaneIndex)
			{
				return true;
			}
			var laneSwitchesRequired = Math.Abs(fromPiecePosition.Lane.EndLaneIndex - toPiecePosition.Lane.EndLaneIndex);
			var fromPiece = raceData.GetPiece(fromPiecePosition.PieceIndex);
			var toPiece = raceData.GetPiece(toPiecePosition.PieceIndex);
			return fromPiece.SwitchesBetween(toPiece) >= laneSwitchesRequired;
		}

		public bool CanReachOnStraightLine(PiecePosition fromPiecePosition, PiecePosition toPiecePosition)
		{
			var fromPiece = raceData.GetPiece(fromPiecePosition.PieceIndex);
			var toPiece = raceData.GetPiece(toPiecePosition.PieceIndex);
			return CanReach(fromPiecePosition, toPiecePosition) && !fromPiece.IsCurve() && fromPiece.CurvesBetween(toPiece) == 0;
		}

		// assuming car in front is aiming for angle 60
		public bool CanCatchBeforeInCurve(CarPosition backCarPosition, CarPosition frontCarPosition, bool assumeBackCarTurbo, bool assumeFrontCarTurbo)
		{
			if (!raceData.Constants.VMax.HasValue || !raceData.Constants.Horsepower.HasValue ||
					!raceData.Constants.Resistance.HasValue)
			{
				return false;
			}
			var backCarPiece = raceData.GetPiece(backCarPosition.PiecePosition.PieceIndex);
			var frontCarPiece = raceData.GetPiece(frontCarPosition.PiecePosition.PieceIndex);
			var frontCarLane = raceData.GetLane(frontCarPosition.PiecePosition.Lane.EndLaneIndex);
			if (backCarPiece == null || frontCarPiece == null || frontCarPiece.IsCurve())
			{
				return false;
			}
			var nextCurve = frontCarPiece.NextCurve();

			int backCarPositionTicks = 0;
			var backCarDistanceToCurve = backCarPiece.DistanceTo(nextCurve, frontCarLane) - backCarPosition.PiecePosition.InPieceDistance;
			var backCarSpeed = backCarPosition.Speed.GetValueOrDefault();
			double backCarTravelledDistance = 0;
			double backCarVmax = raceData.Constants.VMax.GetValueOrDefault();
			if (raceData.LastSeenTurbo != null && assumeBackCarTurbo)
			{
				backCarVmax = backCarVmax * raceData.LastSeenTurbo.TurboFactor;
			}
			while (backCarTravelledDistance < backCarDistanceToCurve)
			{
				// simulate 1 tick with throttle 1
				backCarSpeed += ((backCarVmax - backCarSpeed) / raceData.Constants.Horsepower.Value);
				backCarTravelledDistance += backCarSpeed;
				backCarPositionTicks++;
			}

			int frontCarPositionTicks = 0;
			var frontCarRemainingDistance = frontCarPiece.DistanceTo(nextCurve, frontCarLane) - frontCarPosition.PiecePosition.InPieceDistance;
			var frontCarSpeed = frontCarPosition.Speed.GetValueOrDefault();
			var frontCarTargetSpeed = throttleCalculator.GetMaxSpeedForRadius(nextCurve.GetRadius(frontCarLane));
			double frontCarVmax = raceData.Constants.VMax.GetValueOrDefault();
			if (raceData.LastSeenTurbo != null && assumeFrontCarTurbo)
			{
				frontCarVmax = frontCarVmax * raceData.LastSeenTurbo.TurboFactor;
			}
			while (frontCarRemainingDistance > 0)
			{
				if (throttleCalculator.BreakRequired(frontCarRemainingDistance, frontCarSpeed, frontCarTargetSpeed))
				{
					frontCarRemainingDistance -= frontCarSpeed;
					frontCarSpeed = frontCarSpeed - (frontCarSpeed / raceData.Constants.Resistance.Value);
				}
				else
				{
					frontCarSpeed += ((frontCarVmax - frontCarSpeed) / raceData.Constants.Horsepower.Value);
					frontCarRemainingDistance -= frontCarSpeed;
				}
				frontCarPositionTicks++;
			}

			return backCarPositionTicks <= frontCarPositionTicks;
		}
	}
}