using System;
using System.Diagnostics;
using MrBotLibrary.ApiReponses;
using MrBotLibrary.Extensions;

namespace MrBotLibrary.Calculators.Base
{
	public class ThrottleCalculator
	{
		// TODO: calculate these
		private const double NoDriftFactor = 0.32;
		private const double DriftWithAngle60Factor = 0.45;
		private readonly RaceData raceData;

		public ThrottleCalculator(RaceData raceData)
		{
			this.raceData = raceData;
		}

		// simulates driving with throttle 1 for another few game ticks and then using throttle 0
		public bool BreakRequired(double distance, double currentSpeed, double targetSpeed)
		{
			if (!raceData.Constants.Resistance.HasValue || !raceData.Constants.Horsepower.HasValue ||
			    !raceData.CurrentVmax().HasValue)
			{
				throw new Exception("I don't know if we should break since I don't know the resistance/horsepower/vMax");
			}
			if (Math.Abs(targetSpeed) < 0.1)
			{
				return true;
			}
			double travelledDistance = 0;
			currentSpeed += ((raceData.CurrentVmax().Value - currentSpeed)/raceData.Constants.Horsepower.Value);
			travelledDistance += currentSpeed;
			while (travelledDistance < distance && currentSpeed > targetSpeed)
			{
				// simulate 1 tick with throttle 0;
				travelledDistance += currentSpeed;
				currentSpeed = currentSpeed - (currentSpeed/raceData.Constants.Resistance.Value);
			}
			return travelledDistance >= distance && currentSpeed > targetSpeed;
		}

		public double GetMaxSpeedWithoutDriftForRadius(double radius)
		{
			// (speed * speed) / radius = 0.32 =>
			double speed = Math.Sqrt(NoDriftFactor*radius);
			Debug.WriteLine("Radius: {0} - Max speed without drift: {1}", radius, speed);
			return speed;
		}

		public double GetMaxSpeedForRadius(double radius)
		{
			// (speed * speed) / radius = 0.46 =>
			double speed = Math.Sqrt(DriftWithAngle60Factor*radius);
			Debug.WriteLine("Radius: {0} - Target speed: {1}", radius, speed);
			return speed;
		}

		//public double GetMaxCurveEnteringSpeedForRadius(double radius, double currentAngle, double targetAngle)
		//{
		//	double maxCurveEnteringSpeed = GetMaxSpeedForRadius(radius);
		//	if (raceData.Constants.Resistance.HasValue)
		//	{
		//		double angleDiff = Math.Abs(targetAngle - currentAngle);
		//		if (angleDiff > targetAngle)
		//		{
		//			return maxCurveEnteringSpeed;
		//		}
		//		double breakTicksInCurve = Math.Round(angleDiff/10);
		//		for (int i = 0; i <= breakTicksInCurve; i++)
		//		{
		//			maxCurveEnteringSpeed = (maxCurveEnteringSpeed*raceData.Constants.Resistance.Value)/
		//															(raceData.Constants.Resistance.Value - 1);
		//		}
		//	}
		//	Debug.WriteLine("Radius: {0} - Target entering speed: {1}", radius, maxCurveEnteringSpeed);
		//	return maxCurveEnteringSpeed;
		//}

		//public double GetMaxCurveEnteringSpeed(double radius, Piece curve, double currentAngle)
		//{
		//	int targetAbsoluteAngle = 60;
		//	double speed = GetMaxCurveEnteringSpeedForRadius(radius, currentAngle,
		//		curve.Angle > 0 ? targetAbsoluteAngle : -targetAbsoluteAngle);
		//	return speed;
		//}
	}
}