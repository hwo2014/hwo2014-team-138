﻿using MrBotLibrary.Interfaces;

namespace MrBotLibrary.Calculators.SwitchLane
{
	public class AggressiveSwitchLaneCalculator : ISwitchLaneCalculator
	{
		private readonly RaceData raceData;

		public AggressiveSwitchLaneCalculator(RaceData raceData)
		{
			this.raceData = raceData;
		}

		public string Calculate()
		{
			// TODO: implement logic to follow slow cars to bump them to death?
			return null;
		}
	}
}