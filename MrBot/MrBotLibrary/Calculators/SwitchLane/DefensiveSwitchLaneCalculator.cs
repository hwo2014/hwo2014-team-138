﻿using System;
using System.Linq;
using MrBotLibrary.ApiReponses;
using MrBotLibrary.Calculators.Base;
using MrBotLibrary.Extensions;
using MrBotLibrary.Interfaces;

namespace MrBotLibrary.Calculators.SwitchLane
{
	public class DefensiveSwitchLaneCalculator : ISwitchLaneCalculator
	{
		private readonly RaceData raceData;
		private readonly OpponentAwareCalculator opponentAwareCalculator;

		public DefensiveSwitchLaneCalculator(RaceData raceData)
		{
			this.raceData = raceData;
			opponentAwareCalculator = new OpponentAwareCalculator(raceData);
		}

		public string Calculate()
		{
			CarPosition ownCurrentPosition = raceData.OwnCarPosition();
			var ownCurrentPiece = raceData.GetPiece(ownCurrentPosition.PiecePosition.PieceIndex);
			var ownCurrentLane = raceData.GetLane(ownCurrentPosition.PiecePosition.Lane.EndLaneIndex);
			var nextPiece = ownCurrentPiece.Next();
			if (!nextPiece.Switch || ownCurrentPosition.PiecePosition.Lane.StartLaneIndex != ownCurrentPosition.PiecePosition.Lane.EndLaneIndex)
			{
				return null;
			}
			foreach (var carPositions in raceData.CarPositionCache)
			{
				CarPosition currentPosition = carPositions.Value.Last();
				if (carPositions.Key.Name == raceData.Self.Name || raceData.CrashedCars.Contains(currentPosition.Id))
				{
					continue;
				}
				bool canBeReached = opponentAwareCalculator.CanReach(currentPosition.PiecePosition, ownCurrentPosition.PiecePosition);
				double distanceToOwnCar = opponentAwareCalculator.CalculateDistance(currentPosition.PiecePosition, ownCurrentPosition.PiecePosition);
				double distanceFromOwnCar = opponentAwareCalculator.CalculateDistance(ownCurrentPosition.PiecePosition, currentPosition.PiecePosition);
				var assumeOpponentTurbo = raceData.LastSeenTurbo != null;
				var assumeOwnTurbo = raceData.Turbo != null;
				if (canBeReached && distanceToOwnCar < distanceFromOwnCar &&
						opponentAwareCalculator.CanCatchBeforeInCurve(currentPosition, ownCurrentPosition, assumeOpponentTurbo, assumeOwnTurbo))
				{
					var shortestLanes = nextPiece.ShortestLanes.Where(x => !x.Equals(ownCurrentLane)).ToArray();
					var otherLanes = raceData.Track.Lanes.Where(x => !shortestLanes.Contains(x) && !x.Equals(ownCurrentLane)).ToArray();
					if (shortestLanes.Any())
					{
						var lane = shortestLanes.OrderBy(x => Math.Abs(x.DistanceFromCenter - ownCurrentLane.DistanceFromCenter)).First();
							return lane.Index < ownCurrentLane.Index ? "Left" : "Right";
					}
					if (otherLanes.Any())
					{
						var lane = otherLanes.OrderBy(x => Math.Abs(x.DistanceFromCenter - ownCurrentLane.DistanceFromCenter)).First();
						return lane.Index < ownCurrentLane.Index ? "Left" : "Right";
					}
				}
			}
			return null;
		}
	}
}