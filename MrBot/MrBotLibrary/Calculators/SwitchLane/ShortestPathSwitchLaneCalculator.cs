﻿using System.Linq;
using MrBotLibrary.ApiReponses;
using MrBotLibrary.Extensions;
using MrBotLibrary.Interfaces;

namespace MrBotLibrary.Calculators.SwitchLane
{
	public class ShortestPathSwitchLaneCalculator : ISwitchLaneCalculator
	{
		private readonly RaceData raceData;

		public ShortestPathSwitchLaneCalculator(RaceData raceData)
		{
			this.raceData = raceData;
		}

		public string Calculate()
		{
			CarPosition ownCurrentPosition = raceData.OwnCarPosition();
			Piece ownCurrentPiece = raceData.GetPiece(ownCurrentPosition.PiecePosition.PieceIndex);
			Lane ownCurrentLane = raceData.GetLane(ownCurrentPosition.PiecePosition.Lane.EndLaneIndex);
			Piece nextPiece = ownCurrentPiece.Next();
			if (!nextPiece.Switch || nextPiece.ShortestLanes.Contains(ownCurrentLane))
			{
				return null;
			}
			if (ownCurrentLane.DistanceFromCenter > nextPiece.ShortestLanes.Min(x => x.DistanceFromCenter))
			{
				return "Left";
			}
			if (ownCurrentLane.DistanceFromCenter < nextPiece.ShortestLanes.Min(x => x.DistanceFromCenter))
			{
				return "Right";
			}
			return null;
		}
	}
}