namespace MrBotLibrary.Calculators.Throttle
{
	using System.Collections.Generic;
	using System.Linq;

	using MrBotLibrary.ApiReponses;
	using MrBotLibrary.Calculators.Base;
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class AggressiveThrottleCalculator : IThrottleCalculator
	{
		private readonly RaceData raceData;
		private readonly OpponentAwareCalculator opponentAwareCalculator;

		public AggressiveThrottleCalculator(RaceData raceData)
		{
			this.raceData = raceData;
			opponentAwareCalculator = new OpponentAwareCalculator(raceData);
		}

		public string Name
		{
			get { return "Aggressive"; }
		}

		public double? Calculate()
		{
			var ownCurrentPosition = raceData.OwnCarPosition();
			foreach (KeyValuePair<CarId, List<CarPosition>> carPositions in raceData.CarPositionCache)
			{
				var currentPosition = carPositions.Value.Last();
				if (carPositions.Key.Name == raceData.Self.Name || raceData.CrashedCars.Contains(currentPosition.Id))
				{
					continue;
				}
				var canReachOnStraightLine = opponentAwareCalculator.CanReachOnStraightLine(ownCurrentPosition.PiecePosition, currentPosition.PiecePosition);
				var distanceToOwnCar = opponentAwareCalculator.CalculateDistance(currentPosition.PiecePosition, ownCurrentPosition.PiecePosition);
				var distanceFromOwnCar = opponentAwareCalculator.CalculateDistance(ownCurrentPosition.PiecePosition, currentPosition.PiecePosition);
				var assumeOwnTurbo = raceData.Turbo != null;
				var assumeOpponentTurbo = raceData.LastSeenTurbo != null;
				if (canReachOnStraightLine && distanceToOwnCar > distanceFromOwnCar &&
				    opponentAwareCalculator.CanCatchBeforeInCurve(ownCurrentPosition, currentPosition, assumeOwnTurbo, assumeOpponentTurbo))
				{
					return 1;
				}
			}
			return null;
		}
	}
}