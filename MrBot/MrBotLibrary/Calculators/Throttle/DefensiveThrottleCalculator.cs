namespace MrBotLibrary.Calculators.Throttle
{
	using System.Collections.Generic;
	using System.Linq;

	using MrBotLibrary.ApiReponses;
	using MrBotLibrary.Calculators.Base;
	using MrBotLibrary.Extensions;

	public class DefensiveThrottleCalculator : DriftThrottleCalculator
	{
		private readonly RaceData raceData;
		private readonly OpponentAwareCalculator opponentAwareCalculator;
		private readonly ThrottleCalculator throttleCalculator;

		public DefensiveThrottleCalculator(RaceData raceData)
			: base(raceData)
		{
			this.raceData = raceData;
			opponentAwareCalculator = new OpponentAwareCalculator(raceData);
			throttleCalculator = new ThrottleCalculator(raceData);
		}

		public override string Name
		{
			get { return "Defensive"; }
		}

		public override double? Calculate()
		{
			var ownCurrentPosition = raceData.OwnCarPosition();
			var ownCurrentPiece = raceData.GetPiece(ownCurrentPosition.PiecePosition.PieceIndex);
			var ownCurrentLane = raceData.GetLane(ownCurrentPosition.PiecePosition.Lane.EndLaneIndex);
			var nextCurve = ownCurrentPiece.IsCurve() ? ownCurrentPiece : ownCurrentPiece.NextCurve();
			foreach (KeyValuePair<CarId, List<CarPosition>> carPositions in raceData.CarPositionCache)
			{
				var currentPosition = carPositions.Value.Last();
				if (carPositions.Key.Name == raceData.Self.Name || raceData.CrashedCars.Contains(currentPosition.Id))
				{
					continue;
				}
				var canBeReached = opponentAwareCalculator.CanReach(currentPosition.PiecePosition, ownCurrentPosition.PiecePosition);
				var distanceToOwnCar = opponentAwareCalculator.CalculateDistance(currentPosition.PiecePosition, ownCurrentPosition.PiecePosition);
				var distanceFromOwnCar = opponentAwareCalculator.CalculateDistance(ownCurrentPosition.PiecePosition, currentPosition.PiecePosition);
				var assumeOpponentTurbo = raceData.LastSeenTurbo != null;
				var assumeOwnTurbo = raceData.Turbo != null;
				if (canBeReached && distanceToOwnCar < distanceFromOwnCar &&
				    opponentAwareCalculator.CanCatchBeforeInCurve(currentPosition, ownCurrentPosition, assumeOpponentTurbo, assumeOwnTurbo))
				{
					var targetMaxSpeed = throttleCalculator.GetMaxSpeedForRadius(nextCurve.GetRadius(ownCurrentLane));
					return base.Calculate(targetMaxSpeed);
				}
			}
			return null;
		}
	}
}