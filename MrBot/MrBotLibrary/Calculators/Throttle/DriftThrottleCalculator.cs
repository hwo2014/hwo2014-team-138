namespace MrBotLibrary.Calculators.Throttle
{
	using System;
	using System.Linq;

	using MrBotLibrary.ApiReponses;
	using MrBotLibrary.Calculators.Base;
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class DriftThrottleCalculator : IThrottleCalculator
	{
		public virtual string Name
		{
			get { return "Drift"; }
		}
		private readonly RaceData raceData;
		private readonly ThrottleCalculator throttleCalculator;

		public virtual double? Calculate()
		{
			return Calculate(null);
		}
		public virtual double? Calculate(double? targetSpeed)
		{
			var currentPiece = raceData.CurrentPiece();
			var ownCarPosition = raceData.OwnCarPosition();
			var currentLane = raceData.CurrentLane();
			return Calculate(currentPiece, ownCarPosition, currentLane, targetSpeed);
		}

		protected virtual double Calculate(Piece piece, CarPosition carPosition, Lane lane, double? targetSpeed = null)
		{
			var nextCurve = piece.NextCurve();
			var distanceToCurve = piece.DistanceOnShortestLaneTo(nextCurve) - carPosition.PiecePosition.InPieceDistance;
			var nextCurveRadius = (int)nextCurve.GetRadius(nextCurve.ShortestLanes.Contains(lane) ? lane : nextCurve.ShortestLanes.First());
			var nextCurve2 = nextCurve.NextCurve();
			var distanceToCurve2 = piece.DistanceOnShortestLaneTo(nextCurve2) - carPosition.PiecePosition.InPieceDistance;
			var nextCurveRadius2 = (int)nextCurve2.GetRadius(nextCurve2.ShortestLanes.Contains(lane) ? lane : nextCurve2.ShortestLanes.First());
			var nextCurve3 = nextCurve2.NextCurve();
			var distanceToCurve3 = piece.DistanceOnShortestLaneTo(nextCurve3) - carPosition.PiecePosition.InPieceDistance;
			var nextCurveRadius3 = (int)nextCurve3.GetRadius(nextCurve3.ShortestLanes.Contains(lane) ? lane : nextCurve3.ShortestLanes.First());
			double? targetSpeed2 = targetSpeed;
			double? targetSpeed3 = targetSpeed;
			if (!targetSpeed.HasValue)
			{
				targetSpeed = throttleCalculator.GetMaxSpeedForRadius(nextCurveRadius);
				targetSpeed2 = throttleCalculator.GetMaxSpeedForRadius(nextCurveRadius2);
				targetSpeed3 = throttleCalculator.GetMaxSpeedForRadius(nextCurveRadius3);
			}
			if (raceData.Constants.Resistance.HasValue && (throttleCalculator.BreakRequired(distanceToCurve, carPosition.Speed.GetValueOrDefault(), targetSpeed.Value) || throttleCalculator.BreakRequired(distanceToCurve2, carPosition.Speed.GetValueOrDefault(), targetSpeed2.Value) || throttleCalculator.BreakRequired(distanceToCurve3, carPosition.Speed.GetValueOrDefault(), targetSpeed3.Value)))
			{
				return 0;
			}
			if (piece.IsCurve())
			{
				var currentRadius = (int)piece.GetRadius(lane);
				var targetMaxSpeed = throttleCalculator.GetMaxSpeedForRadius(currentRadius);
				if (!carPosition.Speed.HasValue)
				{
					return 0;
				}
				if (carPosition.Speed.Value > targetMaxSpeed)
				{
					return 0;
				}
				if (Math.Abs(carPosition.Speed.Value - targetMaxSpeed) < 0.1)
				{
					return targetMaxSpeed / 10;
				}
				return 1;
			}
			return 1;
		}

		public DriftThrottleCalculator(RaceData raceData)
		{
			this.raceData = raceData;
			throttleCalculator = new ThrottleCalculator(raceData);
		}
	}
}