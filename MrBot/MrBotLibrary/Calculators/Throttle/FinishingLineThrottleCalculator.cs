namespace MrBotLibrary.Calculators.Throttle
{
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class FinishingLineThrottleCalculator : IThrottleCalculator
	{
		public string Name
		{
			get { return "Finish"; }
		}
		private readonly RaceData raceData;

		public double? Calculate()
		{
			if (raceData.OwnCarPosition().PiecePosition.Lap + 1 == raceData.RaceSession.Laps && raceData.CurrentPiece().IsOnHomeStrech)
			{
				return 1;
			}
			return null;
		}
		public FinishingLineThrottleCalculator(RaceData raceData)
		{
			this.raceData = raceData;
		}
	}
}