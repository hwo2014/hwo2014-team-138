﻿namespace MrBotLibrary.Calculators.Throttle
{
	using System;

	using MrBotLibrary.ApiReponses;
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class GatherDataThrottleCalculator : IThrottleCalculator
	{
		public string Name
		{
			get { return "Science"; }
		}
		private readonly RaceData raceData;

		public GatherDataThrottleCalculator(RaceData raceData)
		{
			this.raceData = raceData;
		}

		public double? Calculate()
		{
			foreach (CarId car in raceData.CarPositionCache.Keys)
			{
				TryCalculateSpeedAndAcceleration(raceData.CurrentCarPosition(car), raceData.LastCarPosition(car));
			}
			TryCalculateMaxSpeedAndHorsePower(raceData.OwnCarPosition(), raceData.LastCarPosition(raceData.Self));
			TryCalculateResistance(raceData.OwnCarPosition(), raceData.LastCarPosition(raceData.Self));
			// full speed to find out what's our top speed / horsepower
			if (!raceData.Constants.VMax.HasValue || !raceData.Constants.Horsepower.HasValue)
			{
				// for science!
				return 1;
			}
			// break to find out what's the resistance
			if (!raceData.Constants.Resistance.HasValue)
			{
				return 0;
			}
			return null;
		}

		protected void TryCalculateSpeedAndAcceleration(CarPosition carPosition, CarPosition previousCarPosition)
		{
			if (previousCarPosition == null || carPosition == null)
			{
				return;
			}
			double travelledDistance;
			if (previousCarPosition.PiecePosition.PieceIndex == carPosition.PiecePosition.PieceIndex)
			{
				travelledDistance = carPosition.PiecePosition.InPieceDistance - previousCarPosition.PiecePosition.InPieceDistance;
			}
			else
			{
				var lastPiece = raceData.GetPiece(previousCarPosition.PiecePosition.PieceIndex);
				var lastLane = raceData.GetLane(previousCarPosition.PiecePosition.Lane.EndLaneIndex);
				var travelledDistanceOfLastPiece = lastPiece.GetLength(lastLane) - previousCarPosition.PiecePosition.InPieceDistance;
				var travelledDistanceOfCurrentPiece = carPosition.PiecePosition.InPieceDistance;
				travelledDistance = travelledDistanceOfCurrentPiece + travelledDistanceOfLastPiece;
			}
			var travelledGameTicks = Math.Abs(carPosition.GameTick - previousCarPosition.GameTick);
			var speed = travelledDistance / travelledGameTicks;
			carPosition.Speed = speed;
			if (previousCarPosition.Speed.HasValue)
			{
				var speedDelta = carPosition.Speed.Value - previousCarPosition.Speed.Value;
				carPosition.Acceleration = speedDelta / travelledGameTicks;
			}
		}
		protected void TryCalculateResistance(CarPosition carPosition, CarPosition previousCarPosition)
		{
			if ((raceData.Constants.VMax.HasValue && raceData.Constants.Horsepower.HasValue && !raceData.Constants.Resistance.HasValue) && carPosition.Speed.HasValue && carPosition.Acceleration.HasValue && carPosition.Acceleration.Value < 0 && Math.Abs(previousCarPosition.Throttle.GetValueOrDefault()) < 0.0001)
			{
				raceData.Constants.Resistance = Math.Abs(carPosition.Speed.Value / carPosition.Acceleration.Value);
			}
		}
		protected void TryCalculateMaxSpeedAndHorsePower(CarPosition carPosition, CarPosition previousCarPosition)
		{
			if ((!raceData.Constants.VMax.HasValue || !raceData.Constants.Horsepower.HasValue) && carPosition.Speed.GetValueOrDefault() > 0 && carPosition.Acceleration.GetValueOrDefault() > 0 && previousCarPosition != null && previousCarPosition.Speed.GetValueOrDefault() > 0 && previousCarPosition.Acceleration.GetValueOrDefault() > 0 && Math.Abs(previousCarPosition.Throttle.GetValueOrDefault() - 1) < 0.0001)
			{
				raceData.Constants.VMax = ((previousCarPosition.Acceleration.Value * carPosition.Speed.Value) - (previousCarPosition.Speed.Value * carPosition.Acceleration.Value)) / (previousCarPosition.Acceleration.Value - carPosition.Acceleration.Value);
				raceData.Constants.Horsepower = (carPosition.Speed.Value - previousCarPosition.Speed.Value) / (previousCarPosition.Acceleration.Value - carPosition.Acceleration.Value);
			}
		}
	}
}