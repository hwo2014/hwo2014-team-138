﻿namespace MrBotLibrary.Calculators.Turbo
{
	using System.Collections.Generic;
	using System.Linq;

	using MrBotLibrary.ApiReponses;
	using MrBotLibrary.Calculators.Base;
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class AggressiveTurboCalculator : ITurboCalculator
	{
		private readonly RaceData raceData;
		private readonly OpponentAwareCalculator opponentAwareCalculator;

		public AggressiveTurboCalculator(RaceData raceData)
		{
			this.raceData = raceData;
			opponentAwareCalculator = new OpponentAwareCalculator(raceData);
		}

		public bool? Calculate()
		{
			// if we can push someone to death return true
			var ownCurrentPosition = raceData.OwnCarPosition();
			foreach (KeyValuePair<CarId, List<CarPosition>> carPositions in raceData.CarPositionCache)
			{
				var currentPosition = carPositions.Value.Last();
				if (carPositions.Key.Name == raceData.Self.Name || raceData.CrashedCars.Contains(currentPosition.Id))
				{
					continue;
				}
				var currentPiece = raceData.GetPiece(currentPosition.PiecePosition.PieceIndex);
				var canReachOnStraightLine = opponentAwareCalculator.CanReachOnStraightLine(ownCurrentPosition.PiecePosition, currentPosition.PiecePosition);
				var distanceToOwnCar = opponentAwareCalculator.CalculateDistance(currentPosition.PiecePosition, ownCurrentPosition.PiecePosition);
				var distanceFromOwnCar = opponentAwareCalculator.CalculateDistance(ownCurrentPosition.PiecePosition, currentPosition.PiecePosition);
				var curveInReach = currentPiece.DistanceOnShortestLaneTo(currentPiece.NextCurve()) < raceData.Constants.VMax.GetValueOrDefault() * 25;
				if (curveInReach && canReachOnStraightLine && distanceToOwnCar > distanceFromOwnCar &&
				    opponentAwareCalculator.CanCatchBeforeInCurve(ownCurrentPosition, currentPosition, true, true))
				{
					return true;
				}
			}
			return null;
		}
	}
}