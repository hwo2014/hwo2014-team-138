﻿namespace MrBotLibrary.Calculators.Turbo
{
	using MrBotLibrary.Calculators.Throttle;
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class CasualTurboCalculator : ITurboCalculator
	{
		private readonly RaceData raceData;
		private readonly DriftThrottleCalculator driftThrottleCalculator;

		public CasualTurboCalculator(RaceData raceData)
		{
			this.raceData = raceData;
			driftThrottleCalculator = new DriftThrottleCalculator(raceData);
		}

		public bool? Calculate()
		{
			// if we race alone or in qualifying/warmup phase and we are on the beginning of the longest straight line
			var currentOwnPosition = raceData.CurrentCarPosition(raceData.Self);
			var currentOwnPiece = raceData.GetPiece(currentOwnPosition.PiecePosition.PieceIndex);
			if (currentOwnPiece.Equals(raceData.Track.StartOfLongestStraightLine) && raceData.RaceSession.Laps == 0)
			{
				var driftThrottle = driftThrottleCalculator.Calculate();
				if (driftThrottle.HasValue && driftThrottle > 0.5)
				{
					return true;
				}
			}
			return null;
		}
	}
}