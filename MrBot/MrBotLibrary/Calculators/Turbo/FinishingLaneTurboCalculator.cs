﻿namespace MrBotLibrary.Calculators.Turbo
{
	using MrBotLibrary.Extensions;
	using MrBotLibrary.Interfaces;

	public class FinishingLaneTurboCalculator : ITurboCalculator
	{
		private readonly RaceData raceData;

		public FinishingLaneTurboCalculator(RaceData raceData)
		{
			this.raceData = raceData;
		}

		public bool? Calculate()
		{
			if (raceData.OwnCarPosition().PiecePosition.Lap + 1 == raceData.RaceSession.Laps && raceData.CurrentPiece().IsOnHomeStrech)
			{
				return true;
			}
			return null;
		}
	}
}