namespace MrBotLibrary
{
	public class Constants
	{
		/// <summary>
		/// vMax = (acc1*v2-v1*acc2)/acc1-acc2
		/// </summary>
		public double? VMax { get; set; }
		/// <summary>
		/// When Throttle is 0 then the deceleration in speed will be speed / resistance
		/// </summary>
		public double? Resistance { get; set; }
		/// <summary>
		/// When Throttle is 1 then the acceleration in speed will be (maximum speed - speed) / horsepower
		/// </summary>
		/// <remarks>
		/// (vMax - v) / hp = acc
		/// horsepower = (v2-v1) / (acc1-acc2)
		/// </remarks>
		public double? Horsepower { get; set; }
	}
}