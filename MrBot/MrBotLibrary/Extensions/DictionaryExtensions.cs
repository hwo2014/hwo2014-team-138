﻿using System.Collections.Generic;
using MrBotLibrary.Output;
using System.Linq;

namespace MrBotLibrary.Extensions
{
	public static class DictionaryExtensions
	{
		 public static void OutputAsTable(this Dictionary<string, object> dct)
		 {
			 var table = new ConsoleTable(dct.Keys.ToArray());
			 table.AddRow(dct.Values.ToArray());
			 table.Write();
		 }
	}
}