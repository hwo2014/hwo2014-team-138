﻿namespace MrBotLibrary.Extensions
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using MrBotLibrary.ApiReponses;

	public static class PieceExtensions
	{
		public static Piece Next(this Piece piece)
		{
				var pieceIndex = Array.IndexOf(piece.Track.Pieces, piece);
				return piece.Track.Pieces.Last() == piece ? piece.Track.Pieces.First() : piece.Track.Pieces[pieceIndex + 1];
		}
		public static Piece Prev(this Piece piece)
		{
			var pieceIndex = Array.IndexOf(piece.Track.Pieces, piece);
			return piece.Track.Pieces.First() == piece ? piece.Track.Pieces.Last() : piece.Track.Pieces[pieceIndex - 1];
		}
		public static bool IsCurve(this Piece piece)
		{
			return Math.Abs(piece.Radius) > 0;
		}
		public static Piece NextCurve(this Piece piece)
		{
			Piece next = piece.Next();
			while (!next.IsCurve())
			{
				next = next.Next();
			}
			return next;
		}
		public static Piece NextSwitch(this Piece piece)
		{
			Piece next = piece.Next();
			while (!next.Switch)
			{
				next = next.Next();
			}
			return next;
		}
		public static Piece PrevSwitch(this Piece piece)
		{
			Piece prev = piece.Prev();
			while (!prev.Switch)
			{
				prev = prev.Prev();
			}
			return prev;
		}
		public static List<Lane> GetShortestLanes(this Piece piece)
		{
			var startOfLane = piece.Switch ? piece.Next() : piece.PrevSwitch().Next();
			var endOfLane = piece.NextSwitch();
			double? minDistance = null;
			var minLanes = new List<Lane>();
			foreach (var lane in piece.Track.Lanes)
			{
				var distance = startOfLane.DistanceTo(endOfLane, lane);
				if (!minDistance.HasValue || distance <= minDistance)
				{
					if (distance < minDistance)
					{
						minLanes.Clear();
					}
					minDistance = distance;
					minLanes.Add(lane);
				}
			}
			return minLanes;
		}

		public static double DistanceTo(this Piece fromPiece, Piece toPiece, Lane lane)
		{
			if (fromPiece == toPiece)
			{
				return 0;
			}
			double distance = fromPiece.GetLength(lane);
			var next = fromPiece.Next();
			while (next != toPiece)
			{
				distance += next.GetLength(lane);
				next = next.Next();
			}
			return distance;
		}
		public static double DistanceOnShortestLaneTo(this Piece fromPiece, Piece toPiece)
		{
			double distance = fromPiece.GetLength(fromPiece.ShortestLanes.First());
			var next = fromPiece.Next();
			while (next != toPiece)
			{
				distance += next.GetLength(next.ShortestLanes.First());
				next = next.Next();
			}
			return distance;
		}
		public static double GetLength(this Piece piece, Lane lane)
		{
			if (piece.Length > 0)
			{
				return piece.Length;
			}
			var length = 2 * Math.PI * piece.GetRadius(lane) * (Math.Abs(piece.Angle) / 360);
			return length;
		}
		public static double GetRadius(this Piece piece, Lane lane)
		{
			var radiusDifference = piece.Angle < 0 ? lane.DistanceFromCenter : -lane.DistanceFromCenter;
			var radius = piece.Radius + radiusDifference;
			return radius;
		}

		public static int SwitchesBetween(this Piece piece, Piece targetPiece)
		{
			int switches = 0;
			var nextPiece = piece;
			while (nextPiece != targetPiece)
			{
				if (nextPiece.Switch)
				{
					switches++;
				}
				nextPiece = nextPiece.Next();
			}
			return switches;
		}
		public static int CurvesBetween(this Piece piece, Piece targetPiece)
		{
			int curves = 0;
			var nextPiece = piece;
			while (nextPiece != targetPiece)
			{
				if (nextPiece.IsCurve())
				{
					curves++;
				}
				nextPiece = nextPiece.Next();
			}
			return curves;
		}
	}
}