﻿using System.Collections.Generic;
using System.Linq;
using MrBotLibrary.ApiReponses;

namespace MrBotLibrary.Extensions
{
	public static class RaceDataExtensions
	{
		public static Piece GetPiece(this RaceData raceData, int pieceIndex)
		{
			return raceData.Track.Pieces[pieceIndex];
		}

		public static Lane GetLane(this RaceData raceData, int laneIndex)
		{
			return raceData.Track.Lanes[laneIndex];
		}

		public static Piece CurrentPiece(this RaceData raceData)
		{
			return raceData.Track.Pieces[raceData.OwnCarPosition().PiecePosition.PieceIndex];
		}

		public static Lane CurrentLane(this RaceData raceData)
		{
			return raceData.Track.Lanes.First(x => x.Index == raceData.OwnCarPosition().PiecePosition.Lane.EndLaneIndex);
		}

		public static double? CurrentVmax(this RaceData raceData)
		{
			return raceData.Turbo != null && raceData.Turbo.Active && raceData.Constants.VMax.HasValue ? raceData.Constants.VMax.GetValueOrDefault()*raceData.Turbo.TurboFactor : raceData.Constants.VMax;
		}

		public static CarPosition OwnCarPosition(this RaceData raceData)
		{
			return raceData.CurrentCarPosition(raceData.Self);
		}

		public static CarPosition CurrentCarPosition(this RaceData raceData, CarId car)
		{
			return raceData.CarPositionCache[car].Last();
		}
		public static CarPosition LastCarPosition(this RaceData raceData, CarId car)
		{
			List<CarPosition> carPositions = raceData.CarPositionCache[car];
			return carPositions.Count > 1 ? carPositions[carPositions.Count - 2] : null;
		}
	}
}