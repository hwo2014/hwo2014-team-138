﻿using System.Linq;
using MrBotLibrary.ApiReponses;

namespace MrBotLibrary.Extensions
{
	using System.Collections.Generic;

	public static class TrackExtensions
	{
		public static void CalculateHomeStrech(this Track track)
		{
			var piece = track.Pieces.First();
			while (!piece.Prev().IsCurve())
			{
				piece = piece.Prev();
				piece.IsOnHomeStrech = true;
			}
		}
		public static Piece GetStartOfHomeStrech(this Track track)
		{
			var piece = track.Pieces.First();
			while (piece.Prev().IsOnHomeStrech)
			{
				piece = piece.Prev();
			}
			return piece;
		}
		public static void CalculateLongestStraightLine(this Track track)
		{
			var longestStraightLineSearchingStart = track.Pieces.First();
			while (!longestStraightLineSearchingStart.Prev().IsCurve())
			{
				longestStraightLineSearchingStart = longestStraightLineSearchingStart.Prev();
			}
			
			
			var piecesToCheck = new List<Piece>();
			//piecesToCheck.Add(longestStraightLineSearchingStart);
			var next = longestStraightLineSearchingStart.Next();
			while (!next.Equals(longestStraightLineSearchingStart))
			{
				piecesToCheck.Add(next);
				next = next.Next();
			}

			track.StartOfLongestStraightLine = longestStraightLineSearchingStart;
			double longestStraightLineLength = longestStraightLineSearchingStart.GetLength(longestStraightLineSearchingStart.ShortestLanes.First());
			double currentLength = longestStraightLineLength;
			var currentStart = longestStraightLineSearchingStart;
			foreach (var piece in piecesToCheck)
			{
				if (!piece.IsCurve() && piece.Prev().IsCurve())
				{
					currentLength = piece.GetLength(piece.ShortestLanes.First());
					currentStart = piece;
				}
				if (!piece.IsCurve() && !piece.Prev().IsCurve())
				{
					currentLength += piece.GetLength(piece.ShortestLanes.First());
				}
				if (currentLength > longestStraightLineLength)
				{
					longestStraightLineLength = currentLength;
					track.StartOfLongestStraightLine = currentStart;
				}
			}
		}
	}
}