﻿namespace MrBotLibrary.Extensions
{
	using MrBotLibrary.ApiReponses;

	public static class TurboExtensions
	{
		public static int? GetRemainingTicks(this Turbo turbo, int gameTick)
		{
			if (!turbo.Active)
			{
				return null;
			}
			var passedTicks = gameTick - turbo.ActivationTick;
			return turbo.TurboDurationTicks - passedTicks;
		}
	}
}