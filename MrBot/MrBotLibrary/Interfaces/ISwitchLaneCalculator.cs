﻿namespace MrBotLibrary.Interfaces
{
	public interface ISwitchLaneCalculator
	{
		string Calculate();
	}
}