﻿namespace MrBotLibrary.Interfaces
{
	public interface IThrottleCalculator
	{
		double? Calculate();
		string Name { get; }
	}
}