﻿namespace MrBotLibrary.Interfaces
{
	public interface ITurboCalculator
	{
		bool? Calculate();
	}
}