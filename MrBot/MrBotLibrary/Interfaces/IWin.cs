﻿namespace MrBotLibrary.Interfaces
{
	public interface IWin
	{
		void JoinRace(string host, int port, string trackName, string password, int carCount);
		void Join(string host, int port);
	}
}