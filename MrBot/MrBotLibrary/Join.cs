﻿using Newtonsoft.Json;

namespace MrBotLibrary
{
	public class Join : SendMsg
	{
		[JsonProperty(PropertyName = "key")] public string Key;
		[JsonProperty(PropertyName = "name")] public string Name;

		public Join(string name, string key)
		{
			Name = name;
			Key = key;
		}

		protected override string MsgType()
		{
			return "join";
		}
	}
}