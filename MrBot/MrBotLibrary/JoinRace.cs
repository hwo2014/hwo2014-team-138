﻿using MrBotLibrary.ApiReponses;
using Newtonsoft.Json;

namespace MrBotLibrary
{
	public class JoinRace : Join
	{
		[JsonProperty(PropertyName = "botId")] public BotId BotId;
		[JsonProperty(PropertyName = "carCount")] public int CarCount;
		[JsonProperty(PropertyName = "password")] public string Password;
		[JsonProperty(PropertyName = "trackName")] public string TrackName;

		public JoinRace(string name, string key, string trackName, string password, int carCount)
			: base(name, key)
		{
			BotId = new BotId(name, key);
			TrackName = trackName;
			Password = password;
			CarCount = carCount;
		}

		protected override string MsgType()
		{
			return "joinRace";
		}
	}
}