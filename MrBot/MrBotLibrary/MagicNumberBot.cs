﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using MrBotLibrary.ApiReponses;
using MrBotLibrary.Interfaces;
using MrBotLibrary.Output;
using Newtonsoft.Json;
using MrBotLibrary.Extensions;

namespace MrBotLibrary
{
	public class MagicNumberBot : Bot
	{
		public MagicNumberBot(string botName, string botKey, string color) : base(botName, botKey, color)
		{
		}

		protected override double CalculateThrottleByDistanceToNextCurve()
		{
			var radiusMaxSpeedDictionary = new Dictionary<int, double>();
			radiusMaxSpeedDictionary[10] = 2;
			radiusMaxSpeedDictionary[40] = 2.3;
			radiusMaxSpeedDictionary[60] = 4.4;
			radiusMaxSpeedDictionary[90] = 6.5;
			radiusMaxSpeedDictionary[110] = 7;
			radiusMaxSpeedDictionary[180] = 8.5;
			radiusMaxSpeedDictionary[190] = 8.8;
			radiusMaxSpeedDictionary[200] = 8.9;
			radiusMaxSpeedDictionary[210] = 9;

			var cF = 0.0;
			var nextCurve = CurrentPiece.NextCurve();
			var distanceToCurve = CurrentPiece.DistanceOnShortestLaneTo(nextCurve);
			
			if (CurrentPiece.Radius > 0 && OwnCarPosition.Speed.HasValue)
			{
				var radius = CurrentPiece.GetRadius(CurrentLane);
				cF = (OwnCarPosition.Speed.Value * OwnCarPosition.Speed.Value) / radius;
			}
			var ratio = CurrentPiece.Radius / Math.Abs(CurrentPiece.Angle + 0.001);
			//data.AddFormat("RATIO: {0}", ratio);
			var minThrottle = OwnCarPosition.Angle > 20 && (ratio < 2) ? 0 : Math.Min(1.1 - cF, 1);
			minThrottle = OwnCarPosition.Speed > 5 ? 0.2 : minThrottle;
			minThrottle = turbo != null && turbo.Active ? 0 : minThrottle;
			if (CurrentPiece.IsCurve())
			{
				var currentRadius = (int)CurrentPiece.GetRadius(CurrentLane);
				var targetMaxSpeed = radiusMaxSpeedDictionary[currentRadius];
				//data.AddFormat("RAD: {0}", currentRadius);
				if (!OwnCarPosition.Speed.HasValue)
				{
					return minThrottle;
				}
				return targetMaxSpeed > OwnCarPosition.Speed.Value ? 1 : minThrottle;
			}
			
			var nextCurveRadius = (int)nextCurve.GetRadius(nextCurve.ShortestLanes.Contains(CurrentLane) ? CurrentLane : nextCurve.ShortestLanes.First());
			var targetSpeed = radiusMaxSpeedDictionary[nextCurveRadius];
			if (resistance.HasValue && BreakRequired(distanceToCurve, OwnCarPosition.Speed.GetValueOrDefault(), targetSpeed))
			{
				return minThrottle;
			}
			return 1;
		}

		protected override bool CalculateUseTurbo()
		{
			var nextCurve = CurrentPiece.NextCurve();
			var distanceToCurve = CurrentPiece.DistanceOnShortestLaneTo(nextCurve);
			return !CurrentPiece.IsCurve() && distanceToCurve > 400;
		}
	}
}