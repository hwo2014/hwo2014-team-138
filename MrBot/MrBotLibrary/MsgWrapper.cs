﻿namespace MrBotLibrary
{
	using System;

	internal class MsgWrapper
	{
		public string msgType { get; set; }
		public Object data { get; set; }
		public string gameId { get; set; }
		public int? gameTick { get; set; }
	}
}