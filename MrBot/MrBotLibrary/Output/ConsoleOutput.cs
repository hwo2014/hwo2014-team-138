﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MrBotLibrary.Output
{
	public class ConsoleOutput : IOutput
	{
		private readonly EventList data;
		private readonly EventList events;
		private bool outputPaused;

		public ConsoleOutput(int eventsLimit, int dataLimit)
		{
			events = new EventList(eventsLimit);
			data = new EventList(dataLimit);
		}

		public void AddEvent(string @event)
		{
			events.Add(@event);
		}

		public void AddData(Dictionary<string, object> dataRow)
		{
			data.Add(dataRow);
		}

		public void Flush()
		{
			OnGametick();
		}

		public void OnGametick()
		{
			try
			{
				var sb = new StringBuilder(data.ToTable().ToString());
				sb.AppendLine(events.ToTable().ToString());
				if (!outputPaused)
				{
					Console.Clear();
					Console.WriteLine(sb.ToString());
				}
				if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Spacebar)
				{
					outputPaused = !outputPaused;
				}
			}
			catch
			{
			}
		}
	}
}