﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MrBotLibrary.Output
{
	public class CsvOutput : IOutput
	{
		private readonly EventList data;
		private readonly EventList events;
		private readonly string path;

		public CsvOutput(string path)
		{
			this.path = path;
			events = new EventList(Int32.MaxValue);
			data = new EventList(Int32.MaxValue);
		}

		public void AddEvent(string @event)
		{
			events.Add(@event);
		}

		public void AddData(Dictionary<string, object> dataRow)
		{
			data.Add(dataRow);
		}

		public void Flush()
		{
			string fileName = Path.Combine(path,
				String.Format("{0}_{1}.csv", DateTime.Now.ToShortDateString(), DateTime.Now.ToString("HH.mm.ss")));
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			data.ToFile(fileName);
		}

		public void OnGametick()
		{
		}
	}
}