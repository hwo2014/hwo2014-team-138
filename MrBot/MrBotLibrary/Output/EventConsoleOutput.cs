﻿using System;
using System.Collections.Generic;

namespace MrBotLibrary.Output
{
	public class EventConsoleOutput : IOutput
	{
		public void AddEvent(string @event)
		{
			Console.WriteLine(@event);
		}

		public void AddData(Dictionary<string, object> dataRow)
		{
		}

		public void Flush()
		{
		}

		public void OnGametick()
		{
		}
	}
}