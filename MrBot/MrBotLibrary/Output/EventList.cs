﻿namespace MrBotLibrary.Output
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text;

	public class EventList
	{
		public EventList(int maxLength)
		{
			this.maxLength = maxLength;
			events = new List<KeyValuePair<DateTime, Dictionary<string, object>>>();
		}

		private readonly List<KeyValuePair<DateTime, Dictionary<string, object>>> events;
		private readonly int maxLength;

		public void Add(string evnt)
		{
			events.Add(new KeyValuePair<DateTime, Dictionary<string, object>>(DateTime.Now, new Dictionary<string, object> { { "Event", evnt } }));
		}
		public void AddFormat(string evnt, params object[] args)
		{
			events.Add(new KeyValuePair<DateTime, Dictionary<string, object>>(DateTime.Now, new Dictionary<string, object> { { "Event", String.Format(evnt, args) } }));
		}

		public void Add(Dictionary<string, object> values)
		{
			events.Add(new KeyValuePair<DateTime, Dictionary<string, object>>(DateTime.Now, values));
		}

		public ConsoleTable ToTable()
		{
			var valueCols = new List<string>();
			var limitedEvents = events.Skip(Math.Max(0, events.Count - maxLength)).Take(maxLength);
			foreach (string key in from evnt in limitedEvents from key in evnt.Value.Keys where !valueCols.Contains(key) select key)
			{
				valueCols.Add(key);
			}
			var tableCols = new List<string>();
			tableCols.Add("Time");
			tableCols.AddRange(valueCols);
			var tbl = new ConsoleTable(tableCols.ToArray());
			foreach (KeyValuePair<DateTime, Dictionary<string, object>> evnt in limitedEvents.OrderBy(e => e.Key))
			{
				var values = valueCols.Select(col => evnt.Value.ContainsKey(col) ? evnt.Value[col] : "").ToList();
				values.Insert(0, evnt.Key.ToString("H:m:s"));
				tbl.AddRow(values.ToArray());
			}
			return tbl;
		}
		public void ToFile(string fileName)
		{
			var valueCols = new List<string>();
			foreach (string key in from evnt in events from key in evnt.Value.Keys where !valueCols.Contains(key) select key)
			{
				valueCols.Add(key);
			}
			var tableCols = new List<string>();
			tableCols.Add("Time");
			tableCols.AddRange(valueCols);
			using (var writer = new StreamWriter(fileName))
			{
				writer.WriteLine(String.Join(";", tableCols));
				foreach (KeyValuePair<DateTime, Dictionary<string, object>> evnt in events.OrderBy(e => e.Key))
				{
					var values = valueCols.Select(col => evnt.Value.ContainsKey(col) ? evnt.Value[col] : "").ToList();
					values.Insert(0, evnt.Key.ToString("H:m:s"));
					writer.WriteLine(String.Join(";", values.Select(x => x.ToString())));
				}
			}
		}
	}
}