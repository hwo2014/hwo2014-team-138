using System.Collections.Generic;

namespace MrBotLibrary.Output
{
	public interface IOutput
	{
		void AddEvent(string @event);
		void AddData(Dictionary<string, object> dataRow);
		void Flush();
		void OnGametick();
	}
}