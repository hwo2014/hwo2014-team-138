﻿namespace MrBotLibrary
{
	internal class Ping : SendMsg
	{
		protected override string MsgType()
		{
			return "ping";
		}
	}
}