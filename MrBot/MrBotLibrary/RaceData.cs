using System.Collections.Generic;
using MrBotLibrary.ApiReponses;

namespace MrBotLibrary
{
	public class RaceData
	{
		public CarId Self { get; set; }
		public List<CarId> CrashedCars { get; set; }
		public Track Track { get; set; }
		public RaceSession RaceSession { get; set; }
		public double? MaxAngle { get; set; }
		public Dictionary<CarId, List<CarPosition>> CarPositionCache { get; set; }
		public Turbo Turbo { get; set; }
		public Constants Constants { get; set; }
		public Turbo LastSeenTurbo { get; set; }

		public RaceData()
		{
			CrashedCars = new List<CarId>();
			CarPositionCache = new Dictionary<CarId, List<CarPosition>>();
			Constants = new Constants();
		}
	}
}