﻿using System;
using Newtonsoft.Json;

namespace MrBotLibrary
{
	public abstract class SendMsg
	{
		public string ToJson()
		{
			return JsonConvert.SerializeObject(new MsgWrapper {msgType = MsgType(), data = MsgData()});
		}

		protected virtual Object MsgData()
		{
			return this;
		}

		protected abstract string MsgType();
	}
}