﻿namespace MrBotLibrary
{
	using System;

	internal class SwitchLane : SendMsg
	{
		public string data;

		public SwitchLane(string direction)
		{
			data = direction;
		}

		protected override Object MsgData()
		{
			return data;
		}

		protected override string MsgType()
		{
			return "switchLane";
		}
	}
}