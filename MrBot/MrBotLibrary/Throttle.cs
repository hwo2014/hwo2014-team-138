﻿namespace MrBotLibrary
{
	using System;

	internal class Throttle : SendMsg
	{
		public double value;

		public Throttle(double value)
		{
			this.value = Math.Max(Math.Min(value, 1), 0);
		}

		protected override Object MsgData()
		{
			return value;
		}

		protected override string MsgType()
		{
			return "throttle";
		}
	}
}