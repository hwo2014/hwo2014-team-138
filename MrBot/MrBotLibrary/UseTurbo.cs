﻿namespace MrBotLibrary
{
	using System;

	internal class UseTurbo : SendMsg
	{
		public string data;

		public UseTurbo(string turboMessage)
		{
			data = turboMessage;
		}

		protected override Object MsgData()
		{
			return data;
		}

		protected override string MsgType()
		{
			return "turbo";
		}
	}
}